#!/usr/bin/env python3

import argparse
import json
import logging
import os
import os.path
import re
import shutil
import sys
import tempfile
import time
import typing

import git
import gitlab as gitlab_module
import openml
import requests
from gitlab import exceptions as gitlab_exceptions
from gitlab.v4 import objects as gitlab_objects

logger = logging.getLogger(__name__)

DATASET_PREFIX = 'openml'
DATASETS_GROUP_ID = 6669610
GIT_USER_NAME = "OpenML Bot"
GIT_USER_EMAIL = "openml@datagit.org"
USER_AGENT = 'OpenML Bot (https://gitlab.com/data/bots/openml)'
GIT_LFS_THRESHOLD = 100 * 1024  # 100 KB
GIT_MAX_FILE_SIZE = 2 * 1024 * 1204 * 1204  # 2 GB
MAX_RETRIES = 10
RETRY_WAIT = 5  # seconds
REQUEST_TIMEOUT = 300  # seconds
KNOWN_BAD_DATASETS = {
    # See: https://github.com/openml/OpenML/issues/1036
    '23383',
    '40769',
    '40770',
    '41147',
    '42435',
    '42706',
    '42708',
    '43034',
    '44004',
    '44021',
    '44538',
    '44539',
    '44540',
    '44541',
    '44542',
    '45583',
    # Too large data file.
    '45575',
    '45667',
    '45668',
    '45669',
    '45672',
    # Data not available.
    '40864',
    '45913',
    '45914',
    '45915',
    '45916',
    '45917',
    '45918',
    '45919',
    '45920',
    '45921',
    '45922',
    '45923',
    '45924',
    '45925',
    '45926',
    '45928',
    '45929',
    '45930',
    '45932',
    '45933',
    '45934',
    '45935',
    '46078',
    '46238',
    '46284',
    '46375',
    '46431',
    '46434',
    '46435',
    '46439',
    '46441',
    '46442',
    '46443',
    '46444',
    '46445',
    '46446',
    '46447',
    '46448',
    '46449',
    '46450',
    '46451',
    '46455',
    '46456',
    '46457',
    '46458',
    '46459',
    '46460',
    '46461',
    '46462',
    '46463',
    '46467',
    '46468',
    '46475',
    '46478',
    '46479',
    '46480',
    '46481',
    '46482',
    '46483',
    '46489',
    '46490',
    '46491',
    '46492',
    '46493',
    '46494',
    '46495',
    '46496',
    '46497',
    '46498',
    '46499',
    '46500',
    '46501',
    '46502',
    '46503',
    '46504',
    '46505',
    '46506',
    '46507',
    '46508',
    '46509',
    '46510',
    '46511',
    '46512',
    '46513',
    '46514',
    '46515',
    '46516',
    '46517',
    '46518',
}

session = requests.Session()

# Order matters, which holds for dicts in Python 3.6+.
LICENSES = {
    re.compile(r'\bCC0\b|\bCCZero\b', flags=re.IGNORECASE): 'CC0',
    re.compile(r'\bpublic\b|\bpublic domain', flags=re.IGNORECASE): 'Public Domain Mark',
    re.compile(r'\bBSD\b.*scikit-learn', flags=re.IGNORECASE): 'BSD 3-clause',
    re.compile(r'\bCC.BY.NC.ND\b', flags=re.IGNORECASE): 'CC BY-NC-ND',
    re.compile(r'\bCC.BY.NC.SA\b', flags=re.IGNORECASE): 'CC BY-NC-SA',
    re.compile(r'\bCC.BY.NC\b', flags=re.IGNORECASE): 'CC BY-NC',
    re.compile(r'\bCC.BY.ND\b', flags=re.IGNORECASE): 'CC BY-ND',
    re.compile(r'\bCC.BY.SA\b', flags=re.IGNORECASE): 'CC BY-SA',
    re.compile(r'\bCC.BY\b', flags=re.IGNORECASE): 'CC BY',
    re.compile(r'\bOpen\s+Database\s+License|\bODbL\b', flags=re.IGNORECASE): 'ODbL',
    re.compile(r'\bOpen\s+Government\s+Licence|\bOGL\b', flags=re.IGNORECASE): 'OGL',
}


def normalize_license(license: typing.Optional[str]) -> typing.Optional[str]:
    if license is None:
        return None

    for license_regex, license_name in LICENSES.items():
        if license_regex.search(license):
            return license_name

    return None


def environ_or_required(key: str) -> typing.Dict[str, typing.Any]:
    if os.environ.get(key, None):
        return {'default': os.environ[key]}
    else:
        return {'required': True}


def get_git_url(project_path: str, username: str, password: str) -> str:
    return f'https://{username}:{password}@gitlab.com/{project_path}.git'


def json_dump(document: typing.Dict, output_path: str) -> None:
    with open(output_path, 'w', encoding='utf8') as output_file:
        # We indent and sort so that diff-ing is easier.
        json.dump(document, output_file, ensure_ascii=False, allow_nan=False, indent=2, sort_keys=True)


def create_readme(project_path: str, dataset_id: str, metadata: typing.Dict, repository_dir: str) -> None:
    output_path = os.path.join(repository_dir, 'README.md')

    with open(output_path, 'w', encoding='utf8') as output_file:
        if metadata.get('name', None):
            output_file.write(f"# OpenML dataset: {metadata['name']}\n\n")
        else:
            output_file.write(f"# OpenML dataset\n\n")

        output_file.write(f"https://www.openml.org/d/{dataset_id}\n\n")

        if metadata.get('status', None) == 'deactivated':
            output_file.write(f"**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**\n\n")
        elif metadata.get('status', None) == 'in_preparation':
            output_file.write(f"**WARNING: This dataset is still in preparation.**\n\n")

        output_file.write(
            f"## Structure\n\n"
            f"The dataset has the following file structure:\n\n"
            f"* `dataset/`\n"
            f"  * `tables/`\n"
        )

        if os.path.exists(os.path.join(repository_dir, 'dataset', 'tables', 'data.csv')):
            output_file.write(
                f"    * [`data.csv`](./dataset/tables/data.csv): CSV file with data\n"
            )

        if os.path.exists(os.path.join(repository_dir, 'dataset', 'tables', 'data.pq')):
            output_file.write(
                f"    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data\n"
            )

        output_file.write(
            f"  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset\n"
            f"  * [`features.json`](./dataset/features.json): OpenML description of table columns\n"
            f"  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)\n\n",
        )

        if metadata.get('description', None):
            output_file.write(f"## Description\n\n{metadata['description']}\n\n")

        output_file.write(
            f"## Contributing\n\n"
            f"This is a [read-only mirror](https://gitlab.com/{project_path}) "
            f"of an [OpenML dataset](https://www.openml.org/d/{dataset_id}). "
            f"Contribute any changes to the dataset there. Alternatively, "
            f"[fork the dataset](https://gitlab.com/{project_path}/-/forks/new) "
            f"or [find an existing fork](https://gitlab.com/{project_path}/-/forks) to contribute to.\n\n"
            f"You can use [issues](https://gitlab.com/data/d/openml/{dataset_id}/-/issues) to discuss "
            f"the dataset and any issues.\n\n"
            f"For more information see [https://datagit.org/](https://datagit.org/).\n\n",
        )


def create_license(metadata: typing.Dict, output_path: str) -> None:
    license = normalize_license(metadata.get('licence', None))

    if license is None:
        return None

    shutil.copyfile(
        os.path.join(os.path.dirname(__file__), 'licenses', license.replace(' ', '-').lower() + '.txt'),
        output_path,
    )


def download_json(url: str, output_path: str = None) -> typing.Dict:
    for i in range(MAX_RETRIES):
        try:
            with session.get(
                url,
                headers={
                    'User-Agent': USER_AGENT,
                },
                allow_redirects=True,
                timeout=REQUEST_TIMEOUT,
            ) as response:
                # Code 412 is added to this list because it seems it happens often that this is a temporary error.
                if response.status_code in [412, 429, 500, 502, 503, 504]:
                    if 'Retry-After' in response.headers:
                        wait_time = int(response.headers['Retry-After'])
                    else:
                        wait_time = RETRY_WAIT
                    time.sleep(wait_time)
                    continue

                response.raise_for_status()

                # We parse both ints and floats as floats to obtain canonical version.
                document = response.json(parse_int=float)

            break

        except requests.ConnectionError:
            # If connection issue, retry/resume.
            logger.debug("Connection issue. Retrying.", exc_info=True)
            time.sleep(RETRY_WAIT)

        except requests.Timeout:
            # If timeout, retry/resume.
            logger.debug("Timeout. Retrying.", exc_info=True)
            time.sleep(RETRY_WAIT)

    else:
        raise Exception("Ran out of retries.")

    if output_path is not None:
        json_dump(document, output_path)

    return document


def gitlab_project_configuration(dataset_id: str, project_name: str, metadata: typing.Dict, repository_dir: str) -> typing.Dict:
    project_configuration = {
        'name': f'openml-{project_name}',
        'path': project_name,
        'default_branch': 'master',
        'issues_access_level': 'enabled',
        'repository_access_level': 'enabled',
        'merge_requests_access_level': 'disabled',
        'builds_access_level': 'disabled',
        'wiki_access_level': 'disabled',
        'snippets_access_level': 'disabled',
        'container_registry_enabled': False,
        'merge_method': 'ff',
        'remove_source_branch_after_merge': True,
        'lfs_enabled': True,
        'request_access_enabled': False,
        'tag_list': ','.join(['dataset', 'datagit', 'openml', 'tabular']),
        'visibility': 'public',
        'packages_enabled': False,
    }

    if metadata.get('name', None):
        project_configuration['description'] = f"OpenML dataset: {metadata['name']} https://www.openml.org/d/{dataset_id}"
    else:
        project_configuration['description'] = f"OpenML dataset https://www.openml.org/d/{dataset_id}"

    if metadata.get('status', None) == 'in_preparation':
       project_configuration['tag_list'] += ',in preparation'  # type: ignore

    for tag, ext in [('csv', 'csv'), ('parquet', 'pq')]:
        if os.path.exists(os.path.join(repository_dir, 'dataset', 'tables', f'data.{ext}')):
            project_configuration['tag_list'] += f',{tag}'  # type: ignore

    return project_configuration


def git_push(remote: git.Remote) -> None:
    # Pushing sometimes fails, but it is a temporary error. So we retry.
    for i in range(MAX_RETRIES):
        try:
            results = remote.push('master')

            if not results:
                logger.debug("Unable to push.")
                time.sleep(RETRY_WAIT)
                continue
            elif len(results) != 1:
                logger.debug("Invalid number of results from push: %(results)s", {'results': len(results)})
                time.sleep(RETRY_WAIT)
                continue
            elif results[0].flags & git.PushInfo.ERROR:
                logger.debug("Unable to push: %(summary)s", {'summary': results[0].summary})
                time.sleep(RETRY_WAIT)
                continue

            break

        except git.GitCommandError as error:
            logger.debug("git error: %(error)s", {'error': str(error)}, exc_info=True)
            time.sleep(RETRY_WAIT)

    else:
        raise Exception("Ran out of retries.")


def create_project(arguments: argparse.Namespace, gitlab: gitlab_module.Gitlab, project_name: str, project_path: str, dataset_id: str) -> None:
    logger.info("Creating dataset %(dataset_id)s at '%(project_path)s'.", {'dataset_id': dataset_id, 'project_path': project_path})

    metadata = download_json(f'https://www.openml.org/api/v1/json/data/{dataset_id}')['data_set_description']

    with tempfile.TemporaryDirectory() as repository_dir:
        project_configuration = gitlab_project_configuration(dataset_id, project_name, metadata, repository_dir)
        project_configuration.update({
            'namespace_id': DATASETS_GROUP_ID,
        })

        gitlab.projects.create(project_configuration, max_retries=MAX_RETRIES, retry_transient_errors=True)

        try:
            repository = git.Repo.init(repository_dir)

            create_readme(project_path, dataset_id, metadata, repository_dir)

            actor = git.Actor(GIT_USER_NAME, GIT_USER_EMAIL)
            origin_url = get_git_url(project_path, arguments.username, arguments.token)

            repository.git.lfs('install')

            repository.git.add(all=True)
            repository.index.commit("Initial commit.", author=actor, committer=actor)
            remote = repository.create_remote('origin', origin_url)

            git_push(remote)

        except Exception as error:
            logger.exception("Error creating a repository for dataset %(dataset_id)s at '%(project_path)s'.", {'dataset_id': dataset_id, 'project_path': project_path})

            # We remove the project to not leave around a non-initialized
            # project (a project without git repository).
            gitlab.projects.delete(project_path, max_retries=MAX_RETRIES, retry_transient_errors=True)
            gitlab.projects.delete(project_path, max_retries=MAX_RETRIES, retry_transient_errors=True, permanently_remove=True, full_path=project_path)

            raise error


def update_project_configuration(arguments: argparse.Namespace, gitlab: gitlab_module.Gitlab, project_name: str, project_path: str, dataset_id: str, metadata: typing.Dict, repository_dir: str) -> None:
    logger.info("Updating project %(dataset_id)s configuration.", {'dataset_id': dataset_id})

    project_configuration = gitlab_project_configuration(dataset_id, project_name, metadata, repository_dir)

    project = gitlab.projects.get(project_path, lazy=True)

    for key, value in project_configuration.items():
        setattr(project, key, value)

    project.save(max_retries=MAX_RETRIES, retry_transient_errors=True)

    if metadata.get('status', None) == 'deactivated':
        project.archive(max_retries=MAX_RETRIES, retry_transient_errors=True)
    else:
        project.unarchive(max_retries=MAX_RETRIES, retry_transient_errors=True)


def download_pq_data(repository_dir: str, dataset_id: str, parquet_url: str) -> None:
    download_data(repository_dir, dataset_id, 'pq', parquet_url)

def download_csv_data(repository_dir: str, dataset_id: str, file_id: str) -> None:
    download_data(repository_dir, dataset_id, 'pq', f'https://www.openml.org/data/v1/get_csv/{file_id}')

def download_data(repository_dir: str, dataset_id: str, ext: str, url: str) -> None:
    logger.debug("Downloading %(ext)s data file at %(url)s for dataset %(dataset_id)s.", {'ext': ext, 'url': url, 'dataset_id': dataset_id})

    with open(os.path.join(repository_dir, 'dataset', 'tables', f'data.{ext}'), 'wb') as output_file:
        downloaded = 0

        for i in range(MAX_RETRIES):
            try:
                headers = {
                    'User-Agent': USER_AGENT,
                }
                if downloaded:
                    headers['Range'] = 'bytes={downloaded}-'.format(downloaded=downloaded)
                with session.get(
                    url,
                    stream=True,
                    headers=headers,
                    allow_redirects=True,
                    timeout=REQUEST_TIMEOUT,
                ) as response:
                    # Code 412 is added to this list because it seems it happens often that this is a temporary error.
                    if response.status_code in [412, 429, 500, 502, 503, 504]:
                        if 'Retry-After' in response.headers:
                            wait_time = int(response.headers['Retry-After'])
                        else:
                            wait_time = RETRY_WAIT
                        time.sleep(wait_time)
                        continue

                    response.raise_for_status()

                    # We require the server to support partial requests. In the case that it returns code 200 when we
                    # have send "Range" header (when downloaded is not zero) we raise an exception.
                    if (downloaded and response.status_code != 206) or (not downloaded and response.status_code != 200):
                        raise requests.HTTPError("Unexpected status: {status_code}".format(status_code=response.status_code), response=response)

                    restart = False
                    for data in response.iter_content(chunk_size=64 * 1024):
                        # Sometimes an error message is returned instead of the data.
                        # See: https://github.com/openml/OpenML/issues/1127
                        if b'Database connection error. Usually due to high server load. Please wait N seconds and try again.' in data:
                            restart = True
                            break

                        downloaded += len(data)
                        output_file.write(data)  # type: ignore

                    if restart:
                        # If database connection issue, restart.
                        output_file.seek(0)
                        output_file.truncate()
                        downloaded = 0
                        logger.debug("Database connection error (%(status_code)s). Restarting.", {'status_code': response.status_code})
                        time.sleep(RETRY_WAIT)
                        continue

                break

            except requests.ConnectionError:
                # If connection issue, retry/resume.
                logger.debug("Connection issue. Retrying.", exc_info=True)
                time.sleep(RETRY_WAIT)

            except requests.Timeout:
                # If timeout, retry/resume.
                logger.debug("Timeout. Retrying.", exc_info=True)
                time.sleep(RETRY_WAIT)

        else:
            raise Exception("Ran out of retries.")


def maybe_update_dataset(arguments: argparse.Namespace, gitlab: gitlab_module.Gitlab, project_name: str, project_path: str, dataset_id: str) -> None:
    logger.info("Checking whether to update dataset %(dataset_id)s at '%(project_path)s'.", {'dataset_id': dataset_id, 'project_path': project_path})

    with tempfile.TemporaryDirectory() as repository_dir:
        origin_url = get_git_url(project_path, arguments.username, arguments.token)

        # Cloning a repository often fails, but it is a temporary error. So we retry.
        for i in range(MAX_RETRIES):
            try:
                # We clone shallow and without git LFS downloaded.
                repository = git.Repo.clone_from(origin_url, repository_dir, recurse_submodule=True, shallow_submodules=True, branch='master', depth=1, env={'GIT_LFS_SKIP_SMUDGE': '1'})
                break

            except git.GitCommandError as error:
                logger.debug("git error: %(error)s", {'error': str(error)}, exc_info=True)
                time.sleep(RETRY_WAIT)

                # Make sure the directory is clean before retrying.
                for filename in os.listdir(repository_dir):
                    shutil.rmtree(os.path.join(repository_dir, filename))

        else:
            raise Exception("Ran out of retries.")

        # It could be that some of metadata files are so large that they are stored in git LFS, so we try to fetch them.
        for file_name in ['metadata', 'features', 'qualities']:
            # We do not have to escape include pattern here because these are simple (known) strings.
            repository.git.lfs('pull', include=f'dataset/{file_name}.json')

        os.makedirs(os.path.join(repository_dir, 'dataset', 'tables'), 0o755, exist_ok=True)

        # Metadata includes hash of data, so if metadata changes, we assume data has changed as well.
        # We could be comparing just the hash, but this is simpler to implement.
        metadata = download_json(
            f'https://www.openml.org/api/v1/json/data/{dataset_id}',
            os.path.join(repository_dir, 'dataset', 'metadata.json'),
        )['data_set_description']
        download_json(
            f'https://www.openml.org/api/v1/json/data/features/{dataset_id}',
            os.path.join(repository_dir, 'dataset', 'features.json'),
        )
        download_json(
            f'https://www.openml.org/api/v1/json/data/qualities/{dataset_id}',
            os.path.join(repository_dir, 'dataset', 'qualities.json'),
        )

        forcing = False
        if not repository.is_dirty(untracked_files=True):
            if not arguments.force_metadata and not arguments.force_data:
                logger.info("Dataset has no changes.")
                return
            else:
                forcing = True
                logger.info("Dataset has no changes, but forcing an update.")
        else:
            logger.info("Dataset has changes, updating.")

        # Make sure we have all git LFS files.
        repository.git.lfs('pull')

        if not forcing or arguments.force_data:
            data = 0

            if 'file_id' in metadata:
                try:
                    download_csv_data(repository_dir, dataset_id, metadata['file_id'])
                    data += 1
                except requests.HTTPError as error:
                    if error.response.status_code != 404:
                        raise

                    # Not all datasets have a CSV file. We ignore those errors.
                    # See: https://github.com/openml/OpenML/issues/1126

                    # Sometimes a file disappears later on, but if we have it, then we just keep what
                    # we already have and do not delete it.
                    # See: https://github.com/openml/OpenML/issues/1084

            if 'minio_url' in metadata or 'parquet_url' in metadata:
                try:
                    download_pq_data(repository_dir, dataset_id, metadata.get('minio_url') or metadata.get('parquet_url'))
                    data += 1
                except requests.HTTPError as error:
                    if error.response.status_code not in [403, 404]:
                        raise

                    # Not all datasets have Parquet data, despite having "minio_url" or "parquet_url" in metadata.
                    # Minio can return 403 for those.

            if not data:
                raise Exception("Dataset has no data.")

        update_project_configuration(arguments, gitlab, project_name, project_path, dataset_id, metadata, repository_dir)

        create_readme(project_path, dataset_id, metadata, repository_dir)
        create_license(metadata, os.path.join(repository_dir, 'LICENSE'))

        for dirpath, dirnames, filenames in os.walk(repository_dir):
            dirnames[:] = [dirname for dirname in dirnames if dirname != '.git']

            for filename in filenames:
                filepath = os.path.join(dirpath, filename)
                file_size = os.path.getsize(filepath)

                if file_size >= GIT_MAX_FILE_SIZE:
                    raise ValueError(f"File '{filepath}' is not smaller than {GIT_MAX_FILE_SIZE} bytes.")
                if file_size >= GIT_LFS_THRESHOLD:
                    repository.git.lfs('track', '--filename', filepath[len(repository_dir) + 1:])

        # Maybe nothing really changed (when forced).
        if not repository.is_dirty(untracked_files=True):
            return

        actor = git.Actor(GIT_USER_NAME, GIT_USER_EMAIL)

        repository.git.add(all=True)
        repository.index.commit("Updating dataset.", author=actor, committer=actor)
        remote = repository.remote('origin')

        project = gitlab.projects.get(project_path, lazy=True)
        try:
            # Call is idempotent, so we simply call it always.
            project.unarchive(max_retries=MAX_RETRIES, retry_transient_errors=True)

            git_push(remote)

        finally:
            if metadata.get('status', None) == 'deactivated':
                project.archive(max_retries=MAX_RETRIES, retry_transient_errors=True)


def sync_dataset(arguments: argparse.Namespace, gitlab: gitlab_module.Gitlab, dataset_id: str) -> None:
    logger.info("Syncing dataset %(dataset_id)s.", {'dataset_id': dataset_id})

    project_name = f'{dataset_id}'
    project_path = f'data/d/openml/{project_name}'

    try:
        project: typing.Optional[gitlab_objects.Project] = gitlab.projects.get(project_path, max_retries=MAX_RETRIES, retry_transient_errors=True)
    except gitlab_exceptions.GitlabGetError as error:
        if error.response_code == 404:
            project = None
        else:
            raise error

    if not project:
        create_project(arguments, gitlab, project_name, project_path, dataset_id)

        # Sleeping a bit for everything to settle on GitLab's side.
        time.sleep(1)

    maybe_update_dataset(arguments, gitlab, project_name, project_path, dataset_id)


def get_available_ids(arguments: argparse.Namespace) -> typing.Sequence[str]:
    openml_datasets = {}
    for status in ['active', 'in_preparation', 'deactivated']:
         for dataset_id, dataset_description in openml.datasets.list_datasets(
             data_id=arguments.dataset_ids or None,
             status=status,
         ).items():
             openml_datasets[str(dataset_id)] = dataset_description

    logger.info("There are %(datasets_count)s datasets.", {'datasets_count': len(openml_datasets)})

    available_ids = []
    for dataset_id, dataset_description in sorted(openml_datasets.items(), key=lambda item: int(item[0])):
        if dataset_id in KNOWN_BAD_DATASETS:
            logger.warning("Skipping a known bad dataset %(dataset_id)s.", {'dataset_id': dataset_id})
            continue

        # Future proofing. For now there is just ARFF (tabular) format and this is what we support.
        if dataset_description.get('format', '').lower() not in ['arff', 'sparse_arff']:
            logger.warning("Skipping dataset %(dataset_id)s. It is not in the ARFF format, but '%(format)s'.", {'dataset_id': dataset_id, 'format': dataset_description.get('format', '')})
            continue

        if not dataset_description.get('NumberOfInstances', 0):
            logger.warning("Skipping dataset %(dataset_id)s. It looks invalid (no instances). It's status is '%(status)s'.", {'dataset_id': dataset_id, 'status': dataset_description.get('status', '')})
            continue

        available_ids.append(dataset_id)

    return available_ids


def main(argv: typing.Sequence):
    parser = argparse.ArgumentParser(prog='bot', description="Synchronizes OpenML datasets.")
    parser.add_argument(
        '-u', '--username',
        help="username to use to connect to GitLab.com, has priority over BOT_USERNAME environment variable, required if the environment variable is not set",
        **environ_or_required('BOT_USERNAME'),
    )
    parser.add_argument(
        '-t', '--token',
        help="token to use to connect to GitLab.com, has priority over BOT_TOKEN environment variable, required if the environment variable is not set",
        **environ_or_required('BOT_TOKEN'),
    )
    parser.add_argument(
        '-d', '--debug', action='store_true', default=bool(os.environ.get('BOT_DEBUG', None)),
        help="set logging level to debug, can leak sensitive data, has priority over BOT_DEBUG environment variable",
    )
    parser.add_argument(
        '--force-metadata', action='store_true', default=bool(os.environ.get('FORCE_METADATA_SYNC', None)),
        help="force metadata sync, even if it looks that it is not necessary, has priority over FORCE_METADATA_SYNC environment variable",
    )
    parser.add_argument(
        '--force-data', action='store_true', default=bool(os.environ.get('FORCE_DATA_SYNC', None)),
        help="force data sync, even if it looks that it is not necessary, has priority over FORCE_DATA_SYNC environment variable",
    )
    parser.add_argument(
        '--output-ids', metavar='FILE', type=argparse.FileType('w', encoding='utf8'), action='store',
        help="only output to a file IDs of available OpenML datasets, use \"-\" for stdout",
    )
    parser.add_argument(
        '--input-ids', metavar='FILE', type=argparse.FileType('r', encoding='utf8'), action='store',
        help="a file of available OpenML dataset IDs, use \"-\" for stdin",
    )
    parser.add_argument(
        '--parallel-index', metavar='I', type=int, action='store',
        help="index of a job when running multiple jobs in parallel"
    )
    parser.add_argument(
        '--parallel-total', metavar='N', type=int, action='store',
        help="total number of jobs when running multiple jobs in parallel"
    )
    parser.add_argument(
        'dataset_ids', metavar='ID', type=int, nargs='*',
        help="OpenML dataset IDs to limit available OpenML dataset IDs to",
    )

    arguments = parser.parse_args(argv[1:])

    if (arguments.parallel_index is not None and arguments.parallel_total is None) or (arguments.parallel_index is None and arguments.parallel_total is not None):
        parser.error("both --parallel-index and --parallel-total are required, or none")

    if arguments.input_ids is not None and arguments.dataset_ids:
        parser.error("both --input-ids and OpenML dataset IDs cannot be used at the same time")

    logging.basicConfig(level=logging.DEBUG if arguments.debug else logging.INFO)

    gitlab = gitlab_module.Gitlab('https://gitlab.com', private_token=arguments.token)

    if arguments.input_ids is not None:
        available_ids: typing.Sequence[str] = [line.strip() for line in arguments.input_ids]

    else:
        available_ids = get_available_ids(arguments)

    if arguments.parallel_total:
        available_ids = available_ids[(arguments.parallel_index - 1)::arguments.parallel_total]
        logger.info("Running in parallel (%(parallel_index)s/%(parallel_total)s), synchronizing %(available_ids)s.", {
            'available_ids': len(available_ids),
            'parallel_index': arguments.parallel_index,
            'parallel_total': arguments.parallel_total,
        })

    if arguments.output_ids is not None:
        for dataset_id in available_ids:
            arguments.output_ids.write(f'{dataset_id}\n')
        return

    has_errored = False

    for dataset_id in available_ids:
        try:
            sync_dataset(arguments, gitlab, dataset_id)
        except:
            has_errored = True
            logger.exception("Exception while syncing dataset %(dataset_id)s.", {'dataset_id': dataset_id})

    if has_errored:
        sys.exit(1)


if __name__ == '__main__':
    main(sys.argv)
