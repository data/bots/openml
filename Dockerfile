FROM python:3.8-slim

ENV DEBIAN_FRONTEND=noninteractive
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

COPY requirements.txt /tmp/requirements.txt

RUN \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends build-essential curl git && \
 curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
 apt-get install --yes --force-yes --no-install-recommends git-lfs && \
 git lfs install && \
 pip3 install -r /tmp/requirements.txt && \
 rm -f /tmp/requirements.txt && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm
