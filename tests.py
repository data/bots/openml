#!/usr/bin/env python3

import unittest

import bot

# A list of all "licence" strings found through OpenML API.
LICENSES = {
    'BSD (from scikit-learn)': 'BSD 3-clause',
    'CC0': 'CC0',
    'CC0 1.0': 'CC0',
    'CC0 Public Domain': 'CC0',
    'CC0 Public Domaine': 'CC0',
    'CC_BY': 'CC BY',
    'CC BY 4.0': 'CC BY',
    'CC_BY-NC': 'CC BY-NC',
    'CC_BY-NC-ND': 'CC BY-NC-ND',
    'CC_BY-NC-SA': 'CC BY-NC-SA',
    'CC BY-NC-SA 4.0': 'CC BY-NC-SA',
    'CC_BY-ND': 'CC BY-ND',
    'CC_BY-SA': 'CC BY-SA',
    'CC BY-SA 4.0': 'CC BY-SA',
    'CCZero': 'CC0',
    'GPL-1': None,
    'GPL-2': None,
    'NA': None,
    'Open Database License (ODbL)': 'ODbL',
    'Open Government Licence (OGL)': 'OGL',
    'public': 'Public Domain Mark',
    'Public': 'Public Domain Mark',
    'public domain': 'Public Domain Mark',
    'Public Domain (CC0)': 'CC0',
    'The data is free to use (when citing the publication).': None,
}


class TestBot(unittest.TestCase):
    def test_normalize_license(self):
        for license, license_name in LICENSES.items():
            self.assertEqual(bot.normalize_license(license), license_name, license)


if __name__ == '__main__':
    unittest.main()
